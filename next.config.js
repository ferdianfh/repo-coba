/** @type {import('next').NextConfig} */
const nextConfig = {
  i18n: {
    locales: ["id", "en"],
    defaultLocale: "en",
  },
  images: {
    domains: [
      "blog.antavaya.com",
      "uat-jarvis.mgbedbank.com",
      "storage.googleapis.com",
      "via.placeholder.com",
      "3.0.75.34",
      "cdn.antavaya.com",
    ],
  },
  compress: true,
  async headers() {
    return [
      {
        source: "/:path*",
        headers: [
          {
            key: "maxAge",
            value: "31536000",
          },
          {
            key: "includeSubDomains",
            value: "true",
          },
          {
            key: "preload",
            value: "true",
          },
        ],
      },
    ];
  },
  optimization: {
    mergeDuplicateChunks: true,
  },
  webpack(config, { isServer }) {
    config.module.rules.push({
      test: /\.svg$/i,
      issuer: /\.[jt]sx?$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
};

module.exports = nextConfig;
