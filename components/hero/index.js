// import Image from "next/image";
import Image from "next/legacy/image";
import React from "react";
import ExploreServiceForm from "../exploreService/exploreServiceForm";
import Style from "./hero.module.scss";

const Hero = () => {
  return (
    <>
      <div className={Style.container}>
        <div className={Style.content}>
          <div className={Style.left__content}>
            <div className={`${Style.text__wrapper} font-poppins`}>
              <p className="text-[#414141] text-[54px] leading-[142px] font-bold">
                Find Your <span className="text-[#2395FF]">Flight</span>
              </p>
              <p className="text-[#979797] text-[18px] leading-[54px]">
                and explore the world with us
              </p>
            </div>

            <div className={Style.outer__img}>
              <Image
                src={"/topDestination/destination.png"}
                alt="Hero Background"
                layout="fill"
                objectFit="cover"
                className={`${Style.inner__img} ${Style.img}`}
              />
            </div>
          </div>
          <div className={Style.right__content}>
            <div className={`${Style.outer__img} ${Style.right__img}`}>
              <Image
                src={"/topDestination/destination.png"}
                alt="Hero Background"
                layout="fill"
                objectFit="cover"
                className={`${Style.inner__img} ${Style.img}`}
              />
            </div>
          </div>
        </div>
      </div>

      <ExploreServiceForm />
    </>
  );
};

export default Hero;
