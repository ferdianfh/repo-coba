import Style from "./header.module.scss";
import Logo from "../logo";
import Menu from "./menu";

export default function Header() {
  return (
    <header className={`${Style.container} font-poppins`}>
      <div className={`${Style.content}`}>
        <Logo />

        <Menu />
      </div>
    </header>
  );
}
