import React from "react";
import Style from "./header.module.scss";
import NotifIcon from "../icons/notification.svg";
import InboxIcon from "../icons/inbox.svg";

const HeaderMobile = (props) => {
  const { headerMobileTitle } = props;
  return (
    <div className="w-full h-[60px] flex flex-row justify-center items-center sticky top-0 z-[9999] bg-white">
      <div className="w-full flex flex-row justify-between items-center px-7">
        <p className="text-black text-[36px] leading-[54px] font-bold">
          {headerMobileTitle}
        </p>
        <div className="flex flex-row items-center gap-[25px]">
          <button>
            <InboxIcon />
          </button>
          <button>
            <NotifIcon />
          </button>
        </div>
      </div>
    </div>
  );
};

export default HeaderMobile;
