import Link from "next/link";
import Style from "./menu.module.scss";

export default function Menu() {
  return (
    <nav className={`${Style.container}`}>
      <div className="w-full h-full flex flex-row justify-end items-center gap-[12px]">
        <ul className="flex flex-row items-center gap-[50px] mx-6 font-medium">
          <Link href="/promo">
            <li className="mx-3">All Promos</li>
          </Link>
          <Link href="/booking">
            <li className="mx-3">My Booking</li>
          </Link>
        </ul>

        <div className="mx-6">
          <button
            className={`${Style.signupBtn} px-[44px] py-[13px] rounded-[10px] bg-[#2395FF] text-white font-bold`}
          >
            Sign Up
          </button>
        </div>
      </div>
    </nav>
  );
}
