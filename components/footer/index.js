import Logo from "../logo";
import IconLocation from "../icons/location.svg";
import IconFB from "../icons/facebook.svg";
import IconTwitter from "../icons/twitter.svg";
import IconIG from "../icons/instagram.svg";
import IconYoutube from "../icons/youtube.svg";
import Style from "./footer.module.scss";
import Link from "next/link";
import Image from "next/image";

export default function Footer() {
  return (
    <footer className={`${Style.container} font-poppins`}>
      <div className={`${Style.content} flex flex-col items-center`}>
        <div className="w-full h-full flex flex-row justify-between px-[33px] mb-[50px]">
          <div className="flex flex-col">
            <Logo />
            <p className="mt-7 w-[300px]">
              Find your Flight and explore the world with us. We will take care
              of the rest
            </p>
          </div>

          <div className="flex flex-col">
            <p className="mb-7 font-semibold text-base">Features</p>
            <ul className="text-sm">
              <Link href="/">
                <li className="mb-5">Find Ticket</li>
              </Link>
              <Link href="/">
                <li className="mb-5">My Booking</li>
              </Link>
              <Link href="/">
                <li className="mb-5">Chat</li>
              </Link>
              <Link href="/">
                <li className="mb-5">Notification</li>
              </Link>
            </ul>
          </div>

          <div className="flex flex-col">
            <p className="mb-7 font-semibold text-base">Download Ankasa App</p>
            <Image
              src="/googlePlay.png"
              alt="Google Play"
              width={200}
              height={60}
              className="mb-5"
            />
            <Image
              src="/appStore.png"
              alt="Apple Store"
              width={200}
              height={60}
              className="mb-5"
            />
          </div>

          <div className="flex flex-col">
            <p className="mb-7 font-semibold text-base">Follow Us</p>
            <div className="flex flex-row justify-center items-center gap-[20px]">
              <Link
                href="https://www.facebook.com"
                passHref={true}
                target="_blank"
              >
                <IconFB />
              </Link>
              <Link
                href="https://www.twitter.com"
                passHref={true}
                target="_blank"
              >
                <IconTwitter />
              </Link>
              <Link
                href="https://www.instagram.com"
                passHref={true}
                target="_blank"
              >
                <IconIG />
              </Link>
              <Link
                href="https://www.youtube.com"
                passHref={true}
                target="_blank"
              >
                <IconYoutube />
              </Link>
            </div>
          </div>
        </div>
        <div className="w-full h-full flex flex-row justify-between items-center px-[33px] ">
          <p>©2022 Ankasa. All Rights Reserved.</p>

          <div className="flex flex-row justify-start items-center gap-[12px]">
            <IconLocation />
            <p>Jakarta, Indonesia</p>
          </div>
        </div>
      </div>
    </footer>
  );
}
