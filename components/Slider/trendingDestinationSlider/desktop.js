import Link from "next/link";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import TrendingDestinationCard from "./card";
import Style from "./trendingDestination.module.scss";
import IconChevron from "../../icons/chevronright.svg";
import Image from "next/image";

const TrendingDestinationDesktop = (props) => {
  const { data } = props;
  return (
    <div className={Style.container__desktop}>
      <div className={Style.content__desktop}>
        <div className="flex flex-row justify-between items-center">
          <div className="flex flex-col justify-center text-left text-black z-[5]">
            <p className="uppercase tracking-[0.3em]">Trending</p>
            <p className="font-poppins text-[24px] leading-[36px] font-medium mt-2 mb-[50px]">
              Trending Destinations
            </p>
          </div>

          <Link href={"/"} className="text-[#2395FF] font-medium font-poppins">
            View all
          </Link>
        </div>

        <div>
          <Swiper slidesPerView={5} spaceBetween={15}>
            {data.map((item, index) => {
              return (
                <SwiperSlide key={index}>
                  <TrendingDestinationCard data={item} />
                </SwiperSlide>
              );
            })}
            {/* <SwiperSlide>
              <div className={Style.card__wrapper}>
                <Image
                  src={"/topDestination/spain.png"}
                  alt="Jepang"
                  layout="fill"
                  objectFit="cover"
                  className={Style.card__img}
                />
                <div className={`${Style.img__layer} font-poppins`}>
                  <p className="text-white text-lg leading-[21px] font-semibold mt-5 ml-8 xl:text-left">
                    Barcelona,
                  </p>
                  <p className="text-white text-4xl leading-[36px] font-semibold mt-2 ml-8 xl:text-left xl:text-3xl">
                    Spain
                  </p>
                  <div className={Style.total__airlines}>
                    <span className="font-bold">12</span> Airlines
                  </div>
                  <div className={Style.price__info}>
                    <p>From $100</p>
                    <div className={Style.icon__wrapper}>
                      <IconChevron />
                    </div>
                  </div>
                </div>
              </div>
            </SwiperSlide> */}
          </Swiper>
        </div>
      </div>
    </div>
  );
};

export default TrendingDestinationDesktop;
