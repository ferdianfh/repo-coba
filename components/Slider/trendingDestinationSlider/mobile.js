import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCards } from "swiper";
import "swiper/css";
import "swiper/css/effect-cards";
import Style from "./trendingDestination.module.scss";
import Link from "next/link";
import TrendingDestinationCard from "./card";

const TrendingDestinationMobile = (props) => {
  const { data } = props;
  return (
    <div className={Style.container}>
      <div className={Style.content}>
        <div className="flex flex-row justify-between items-center">
          <p className="font-poppins text-[18px] leading-[27px] text-left font-medium my-6">
            Trending Destinations
          </p>

          <Link href={"/"} className="text-[#2395FF] font-medium font-poppins">
            View all
          </Link>
        </div>
        <div>
          <Swiper
            effect={"cards"}
            modules={[EffectCards]}
            grabCursor={true}
            // slidesPerView={2.5}
            // spaceBetween={20}
            className="mySwiper"
          >
            {data.map((item, index) => {
              return (
                <SwiperSlide key={index}>
                  <TrendingDestinationCard data={item} />;
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
      </div>
    </div>
  );
};

export default TrendingDestinationMobile;
