import Image from "next/image";
import React from "react";
import Style from "./trendingDestination.module.scss";
import IconChevron from "../../icons/chevronright.svg";

const TrendingDestinationCard = (props) => {
  const { data } = props;
  return (
    <div className={Style.card__wrapper}>
      <Image
        src={data.img}
        alt="Jepang"
        layout="fill"
        objectFit="cover"
        className={Style.card__img}
      />
      <div className={`${Style.img__layer} font-poppins`}>
        <p className="text-white text-lg leading-[21px] font-semibold mt-5 ml-8 xl:text-left">
          {data.city},
        </p>
        <p className="text-white text-4xl leading-[36px] font-semibold mt-2 ml-8 xl:text-left xl:text-3xl">
          {data.country}
        </p>
        <div className={Style.total__airlines}>
          <span className="font-bold">{data.total_airlines}</span> Airlines
        </div>
        <div className={Style.price__info}>
          <p>From ${data.total_price}</p>
          <div className={Style.icon__wrapper}>
            <IconChevron />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TrendingDestinationCard;
