import React from "react";
import TrendingDestinationMobile from "./mobile";
import TrendingDestinationDesktop from "./desktop";

const trendingDestination = [
  {
    city: "Tokyo",
    country: "Japan",
    img: "/topDestination/destination.png",
    total_airlines: 15,
    total_price: 100,
    link: "/",
  },
  {
    city: "Barcelona",
    country: "Spain",
    img: "/topDestination/spain.png",
    total_airlines: 12,
    total_price: 200,
    link: "/",
  },
  {
    city: "Singapore",
    country: "Singapore",
    img: "/topDestination/singapore.png",
    total_airlines: 8,
    total_price: 150,
    link: "/",
  },
  {
    city: "Sydney",
    country: "Australia",
    img: "/topDestination/sydney.png",
    total_airlines: 12,
    total_price: 200,
    link: "/",
  },
  {
    city: "Bali",
    country: "Indonesia",
    img: "/topDestination/bali.png",
    total_airlines: 20,
    total_price: 50,
    link: "/",
  },
];

const TrendingDestinations = (props) => {
  const isDesktop = props.isDesktop;
  return (
    <>
      {isDesktop ? (
        <TrendingDestinationDesktop data={trendingDestination} />
      ) : (
        <TrendingDestinationMobile data={trendingDestination} />
      )}
    </>
  );
};

export default TrendingDestinations;
