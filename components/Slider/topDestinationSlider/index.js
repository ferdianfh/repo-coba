import React from "react";
import { Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import TopDestinationCard from "./card";
import Style from "./topDestinations.module.scss";
import Image from "next/image";

const topDestination = [
  { city: "Paris", img: "/topDestination/paris.png" },
  { city: "Bali", img: "/topDestination/bali.png" },
  { city: "Singapore", img: "/topDestination/singapore.png" },
  { city: "Agra", img: "/topDestination/agra.png" },
  { city: "Sydney", img: "/topDestination/sydney.png" },
  { city: "Japan", img: "/topDestination/bali.png" },
  { city: "Spain", img: "/topDestination/singapore.png" },
  { city: "Paris", img: "/topDestination/paris.png" },
  { city: "Agra", img: "/topDestination/agra.png" },
  { city: "Sydney", img: "/topDestination/sydney.png" },
];
const TopDestinations = (props) => {
  const { isDesktop } = props;
  return (
    <>
      {isDesktop ? (
        <div className={Style.container__desktop}>
          <div className={Style.content__desktop}>
            <Image
              src="/topDestination/top-destination-bg.png"
              alt="Background"
              width={1106}
              height={500}
              className="absolute -z-[3]"
            />
            <div className="flex flex-col m-auto justify-center text-center text-white mt-[67px] z-[5]">
              <p className="uppercase tracking-[0.3em]">Top 10</p>
              <p className="font-poppins text-[24px] leading-[36px] font-medium mt-2 mb-[50px]">
                Top 10 Destinations
              </p>
            </div>
            <Swiper
              slidesPerView={5}
              spaceBetween={10}
              navigation={true}
              modules={[Navigation]}
            >
              {topDestination.map((item, index) => {
                return (
                  <SwiperSlide key={index}>
                    <TopDestinationCard data={item} />
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
        </div>
      ) : (
        <div className={Style.container}>
          <div className={Style.content}>
            <p className="font-poppins text-[18px] leading-[27px] text-left font-medium my-6">
              Top 10 Destinations
            </p>

            <Swiper
              slidesPerView={4}
              spaceBetween={30}
              navigation={false}
              modules={[Navigation]}
            >
              {topDestination.map((item, index) => {
                return (
                  <SwiperSlide key={index}>
                    <TopDestinationCard data={item} />
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
        </div>
      )}
    </>
  );
};

export default TopDestinations;
