import Image from "next/image";
import Link from "next/link";
import React from "react";
import Style from "./topDestinations.module.scss";

const TopDestinationCard = (props) => {
  const { data } = props;
  return (
    <Link href={"/"}>
      <div className={Style.card__wrapper}>
        <div className={Style.img__wrapper}>
          <Image
            src={data.img}
            alt="Bali"
            width={100}
            height={100}
            className={Style.img}
          />
        </div>
        <p className="w-[59px] lg:w-[99px] truncate lg:text-clip font-poppins font-medium text-lg leading-[21px] text-center uppercase mt-3 xl:text-white xl:font-normal">
          {data.city}
        </p>
      </div>
    </Link>
  );
};

export default TopDestinationCard;
