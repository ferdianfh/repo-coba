import React from "react";
import ExploreServiceCard from "../exploreServiceCard";
import Style from "./exploreServiceForm.module.scss";
const ExploreServiceForm = () => {
  return (
    <>
      <div className={Style.container}>
        <div className={Style.content}>
          <ExploreServiceCard />
        </div>
      </div>
    </>
  );
};

export default ExploreServiceForm;
