import { useRouter } from "next/router";
import React from "react";
import IconServiceFlight from "../../icons/service/serviceflight.svg";
import IconServiceHotel from "../../icons/service/servicehotel.svg";
import IconServiceTour from "../../icons/service/servicetour.svg";
import Style from "./exploreServiceCard.module.scss";

const ExploreServiceCard = () => {
  const router = useRouter();
  return (
    <>
      <div className={Style.container}>
        <div className={Style.content__wrapper}>
          <p className="py-3 mx-auto text-[18px] leading-[27px] font-poppins font-semibold">
            What do you need?
          </p>
          <div className={Style.content}>
            <button className={Style.card__wrapper}>
              <div className={Style.card}>
                <IconServiceFlight />
              </div>
              <p className={Style.card__text}>Flight</p>
            </button>
            <button className={Style.card__wrapper}>
              <div className={Style.card}>
                <IconServiceHotel />
              </div>
              <p className={Style.card__text}>Hotel</p>
            </button>
            <button
              className={Style.card__wrapper}
              onClick={() => router.push("/tour")}
            >
              <div className={Style.card}>
                <IconServiceTour />
              </div>
              <p className={Style.card__text}>Tour</p>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ExploreServiceCard;
