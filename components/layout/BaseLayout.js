import Footer from "../footer";
import Header from "../header";
import HeaderMobile from "../header/headerMobile";
import MobileNavigationBar from "../navbar";
import Style from "./layout.module.scss";

export default function BaseLayout(props) {
  const showContent = props.screenWidth !== "";
  const isDesktop = props.screenWidth > 1024;
  const { headerMobileTitle } = props;

  return (
    <>
      {isDesktop ? (
        showContent ? (
          <Header />
        ) : (
          ""
        )
      ) : (
        <HeaderMobile headerMobileTitle={headerMobileTitle} />
      )}
      {showContent && (
        <div
          className={`${Style.base__wrapper} ${
            props.className ? props.className : ""
          }`}
        >
          {props.children}
        </div>
      )}
      {isDesktop && showContent && <Footer />}
      {!isDesktop && showContent && <MobileNavigationBar />}
    </>
  );
}
