import Style from "./tour.module.scss";
export default function BannerTour(props) {
  const isDesktop = props.screenWidth > 1024;
  return (
    <div
      className={`${Style.banner_container}`}
      style={{ backgroundImage: "url('/banner1.jpg')" }}
    >
      <p className="text-white text-6xl font-bold flex flex-row justify-center items-center mx-auto">
        Tour
      </p>
    </div>
  );
}
