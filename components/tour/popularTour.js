import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css/navigation";
import "swiper/css";
import Style from "./tour.module.scss";
import TourCard from "./tourCard";

export default function PopularTour(props) {
  const { isLoading, data, error, isError } = props;

  return (
    <>
      <div className={Style.container__popularTour}>
        {!(isLoading || isError) ? (
          <Swiper
            slidesPerView={3}
            spaceBetween={36}
            modules={[Navigation]}
            navigation={true}
            className="populartourSlider"
          >
            {data.map((item, index) => {
              return (
                <SwiperSlide key={"tour" + index}>
                  <TourCard item={item} className={Style.resultSearch} />
                </SwiperSlide>
              );
            })}
          </Swiper>
        ) : (
          <div className="flex">
            {[1, 2, 3].map((item) => (
              <div className={`${Style.card__wrapper} mx-2`} key={item}>
                <div className={`${Style.card__image} skeleton-box`} />

                <div className={Style.card__body}>
                  <div className="flex flex-col">
                    <div className={`skeleton-box w-4/5 h-[18px] mt-4`} />
                    <div className="w-full mt-4 flex">
                      <div className="skeleton-box w-1/3 h-[12px] mr-2" />
                      <div className="skeleton-box w-1/3 h-[12px]" />
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </>
  );
}
