import Image from "next/image";
import Link from "next/link";
import Style from "./tour.module.scss";
import IconCheck from "../../components/icons/check.svg";

const TourCard = (props) => {
  const { item, showIsPopular } = props;
  return (
    <>
      <div
        className={`${Style.card__wrapper} ${
          props.className ? props.className : ""
        } ${props.style ? { ...props.style } : ""}`}
      >
        <Link
          href={`/tour/${item.name
            .toLowerCase()
            .replace(/ /g, "-")}-${item.code.toString()}`}
        >
          <div className={`${Style.card__image} bg-gray-100`}>
            {showIsPopular && (
              <>
                {item && item.isPopular && (
                  <div className={Style.popularBadge}>POPULAR</div>
                )}
              </>
            )}
            <Image
              src={item.image ? item.image : "/placeholder.png"}
              layout="fill"
              objectFit={item.image ? "cover" : "contain"}
              objectPosition="center center"
              alt={item.name}
              priority={true}
            />
          </div>
          <div className={Style.card__body}>
            <div className="flex w-full h-full">
              <div className="flex flex-col w-full h-full justify-between">
                <div className={Style.card__titleContainer}>
                  <div className={Style.card__title}>{item.name}</div>
                </div>
                <div className={Style.card__desc}>
                  <div className="flex flex-col">
                    <div className="text-xs text-grey-300-color mb-1">
                      Start From
                    </div>
                    <div className="flex items-end">
                      <div className="font-bold text-base text-primary-color leading-none">
                        Rp {item.priceStartFrom.display}
                      </div>
                      <div className="text-xs text-grey-300-color">/pax</div>
                    </div>
                  </div>
                  <div className="flex flex-col">
                    <div className="text-xs text-grey-300-color mb-1">
                      {item.isIncludeAirline || item.isIncludeHotel
                        ? "Included"
                        : ""}
                    </div>
                    <div className="flex items-center">
                      {item.isIncludeAirline && (
                        <>
                          <IconCheck />
                          <div className="text-xs text-grey-300-color leading-none ml-2 mr-4">
                            Flight
                          </div>
                        </>
                      )}
                      {item.isIncludeHotel && (
                        <>
                          <IconCheck />
                          <div className="text-xs text-grey-300-color leading-none ml-2 mr-4">
                            Hotel
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Link>
      </div>
    </>
  );
};

export default TourCard;
