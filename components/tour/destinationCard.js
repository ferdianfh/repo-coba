import Link from "next/link";

export const DestinationCard = (props) => {
  const Style = props.Style;
  const isDesktop = props.screenWidth > 1024;
  const { isLoading, data, error, isError } = props;
  if (isError) console.log(error);
  return (
    <div className={Style.wrapper__destination}>
      {isLoading
        ? [1, 2, 3, 4, 5, 6, 7, 8].map((item) => (
            <div className={`skeleton-box ${Style.card}`} key={item} />
          ))
        : data &&
          data.map((data, index) => {
            return (
              <Link
                href={`tour/search${
                  data.name === "All Regions" ? "" : "?r=" + data.name
                }`}
                key={"destination" + index}
                className={`${Style.card} ${
                  isDesktop ? "" : index > 5 ? Style.hidden : ""
                }`}
                style={{ backgroundImage: "url('" + data.image + "')" }}
              >
                <div className="font-bold text-base">{data.name}</div>
                <div className="text-xs">
                  {data.total} Package{data.total > 1 && "s"}
                </div>
              </Link>

              // <div
              //   className={`${Style.card} ${
              //     isDesktop ? "" : index > 3 ? Style.hidden : ""
              //   }`}
              //   style={{ backgroundImage: "url('" + data.image + "')" }}
              // >
              //   <div className="font-bold text-base">{data.name}</div>
              //   <div className="text-xs">
              //     {data.resultData} Package{data.resultData > 1 && "s"}
              //   </div>
              // </div>
            );
          })}
    </div>
  );
};
