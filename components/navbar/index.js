import Link from "next/link";
import React from "react";
import BookingIcon from "../icons/booking.svg";
import ExploreIcon from "../icons/explore.svg";
import ProfileIcon from "../icons/profile.svg";
import NotifIcon from "../icons/notification.svg";
import InboxIcon from "../icons/inbox.svg";
import Style from "./navbar.module.scss";

const listMenu = [
  {
    name: "Booking",
    icon: <BookingIcon />,
    link: "/booking",
  },
  // {
  //   name: "Notification",
  //   icon: <NotifIcon />,
  //   link: "/notification",
  // },
  {
    name: "",
    icon: (
      <div className={`${Style.explore} rounded-full bg-[#2395FF] p-3`}>
        <ExploreIcon />
      </div>
    ),
    link: "/",
  },
  // {
  //   name: "Inbox",
  //   icon: <InboxIcon />,
  //   link: "/inbox",
  // },
  {
    name: "Login",
    icon: <ProfileIcon />,
    link: "/profile",
  },
];

const MobileNavigationBar = () => {
  return (
    <div
      className={`${Style.wrapper} w-full h-[67px] bg-white  fixed bottom-0 left-0 z-[999]`}
    >
      <div className="w-full h-full max-w-[820px] px-10 py-4">
        <nav className="w-full h-full flex flex-row justify-between items-center ">
          {listMenu.map((menu, index) => {
            return (
              <Link
                key={index}
                href={menu.link}
                className="flex flex-col items-center gap-[5px]"
              >
                <button>{menu.icon}</button>
                <span className="text-[#979797] text-xs leading-[15px]">
                  {menu.name}
                </span>
              </Link>
            );
          })}
        </nav>
      </div>
    </div>
  );
};

export default MobileNavigationBar;
