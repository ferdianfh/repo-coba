import Image from "next/image";
import Link from "next/link";

export default function Logo() {
  return (
    <>
      <Link href="/">
        <Image src="/logoAnkasa.png" alt="logoAnkasa" width={158} height={36} />
      </Link>
    </>
  );
}
