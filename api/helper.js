import Cookies from "js-cookie";
import { getListRegion, getListTour } from ".";

// AUTH
const generateRandomString = (length) => {
  let text = "";
  const { floor, random } = Math;
  const char = "ABCDEFGHIJKLMNOPQRSTUVWXYXabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < length; i++) {
    text += char.charAt(floor(random() * char.length));
  }
  return text;
};

const getDeviceId = new Promise((resolve, reject) => {
  const deviceId = generateRandomString(36);
  resolve(deviceId);
});

export const handleGenerateDeviceId = async () => {
  getDeviceId.then(async (result) => {
    Cookies.set("deviceId", result);
  });
};

// TOUR
export const getMainRegion = async () => {
  const { result } = await getListRegion({
    pageNumber: 1,
    pageSize: 10,
    isMain: true,
  });
  const regionList = [];
  if (result && result.responseData) {
    result.responseData.map((item) => {
      regionList.push(item);
    });
  }
  return regionList;
};

export const getPopularTour = async () => {
  const { result } = await getListTour({
    categories: [
      // "series","umroh","fit",
      // "cozytur"
    ],
    destination: "",
    priceRange: {
      start: -1,
      end: -1,
    },
    duration: {
      start: -1,
      end: -1,
    },
    orders: [{ name: "duration", dir: "asc" }],
    paging: {
      number: 1,
      size: 20,
    },
    isPopular: true,
  });
  const popularTourList = [];
  if (result && result.responseData) {
    result.responseData.map((item) => {
      popularTourList.push(item);
    });
  }
  return popularTourList;
};

export const getSearchTour = async (pageNum, searchQuery) => {
  // const dataPayload = {
  //   categories: [],
  //   destination: searchQuery ? searchQuery : "",
  //   priceRange: {
  //     start: 0,
  //     end: -1,
  //   },
  //   duration: {
  //     start: -1,
  //     end: -1,
  //   },
  //   orders: [{ name: "price", dir: "asc" }],
  //   paging: {
  //     number: pageNum,
  //     size: 10,
  //   },
  // };
  const dataPayload = {
    categories: [],
    destination: "",
    priceRange: {
      start: 1000000,
      end: 44990000,
    },
    duration: {
      start: -1,
      end: -1,
    },
    orders: [{ name: "price", dir: "asc" }],
    paging: {
      number: pageNum,
      size: 4,
    },
  };
  const { result } = await getListTour(dataPayload);
  console.log(result);
  const dataSearchTour = [];
  const dataPaging = { maxPage: 0, countData: 0, totalData: 0 };
  if (result) {
    if (result && result.responseData) {
      result.responseData.map((item) => {
        dataSearchTour.push(item);
      });
    }
    if (result && result.paging) {
      dataPaging.maxPage = result.paging.lastPage;
      dataPaging.currentPage = result.paging.currentPage;
      dataPaging.countData = result.paging.countData;
      dataPaging.totalData = result.paging.totalData;
      dataPaging.hasMore = result.paging.nextPage;
    }
  }
  return { dataSearchTour: dataSearchTour, paging: dataPaging };
};
