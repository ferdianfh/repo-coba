import MainService from "./base";
import FormData from "form-data";

const convertToFormData = (formData, data, previousKey) => {
  if (data instanceof Object) {
    Object.keys(data).forEach((key) => {
      const value = data[key];
      if (value instanceof Blob && !Array.isArray(value)) {
        formData.append(key, value, getFilename(value.name));
      }

      if (value instanceof Object && !Array.isArray(value)) {
        return convertToFormData(formData, value, key);
      }
      if (previousKey) {
        key = `${previousKey}[${key}]`;
      }
      if (Array.isArray(value)) {
        value.forEach((val) => {
          formData.append(`${key}[]`, val);
        });
      } else {
        formData.append(key, value);
      }
    });
  }
};

const handleGeneralError = (error) => console.log("General Error", error);
const handleGETRequest = async (
  api,
  { ...param },
  { ...query },
  noAuth = false
) => {
  // let lang = localStorage.getItem("i18nextLng") ? localStorage.getItem("i18nextLng") : process.env.REACT_APP_DEFAULT_LANG;
  const {
    result: { body: bodyResult },
  } = await MainService(api)
    .doRequest({ params: { ...param }, query: { ...query }, noAuth })
    .then((res) => res)
    .catch((errorGeneral) => {
      handleGeneralError(errorGeneral);
      return {
        result: { result: bodyResult },
        errorJS: errorGeneral,
      };
    });

  return { result: bodyResult };
};

// const handlePOSTRequest = async (
//   api,
//   body,
//   asFormData = false,
//   noAuth = false
// ) => {
//   const formData = new FormData();
//   let actualBody = { ...body };

//   if (asFormData) {
//     // https://stackoverflow.com/a/43101878
//     convertToFormData(formData, body);
//     actualBody = formData;
//   }

//   const {
//     result: {
//       body: { data, error, code },
//     },
//   } = await MainService(api)
//     .doRequest({
//       body: actualBody,
//       hooks: {
//         before({ payload, next }) {
//           const newPayload = { ...payload };
//           if (asFormData) delete newPayload.headers["Content-Type"];
//           next(newPayload);
//         },
//       },
//       noAuth,
//     })
//     .then((result) => result)
//     .catch((errorGeneral) => {
//       handleGeneralError(errorGeneral);
//       return {
//         result: {
//           body: { data: null, error: null },
//         },
//         errorJS: errorGeneral,
//       };
//     });

//   if (error) console.log(error);

//   return {
//     code,
//     data,
//     error,
//   };
// };

const handlePOSTRequest = async (
  api,
  body,
  param,
  asFormData = false,
  noAuth = false
) => {
  // console.log(api, param, query);
  // let lang = localStorage.getItem("i18nextLng")
  //   ? localStorage.getItem("i18nextLng")
  //   : process.env.REACT_APP_DEFAULT_LANG;

  const formData = new FormData();
  let actualBody = { ...body };

  if (asFormData) {
    // eslint-disable-next-line
    for (const key in body) {
      formData.append(key, body[key]);
    }
    actualBody = formData;
  }

  const {
    result: { body: bodyResult },
  } = await MainService(api)
    .doRequest({
      noAuth,
      params: { ...param },
      body: actualBody,
      headers: {
        "Content-Type": "application/json",
        // "accept-language": lang,
      },
      hooks: {
        before({ payload, next }) {
          const newPayload = { ...payload };
          if (asFormData) delete newPayload.headers["Content-Type"];
          next(newPayload);
        },
      },
    })
    .then((res) => res)
    .catch((errorGeneral) => {
      handleGeneralError(errorGeneral);
      return {
        result: { result: bodyResult },
        errorJS: errorGeneral,
      };
    });

  return { result: bodyResult };
};

/** Edit this part */
export const getToken = (data) => handlePOSTRequest("getToken", data);

//IDENTITY
export const authLogin = (body) =>
  handlePOSTRequest("authLogin", body, false, false, true);

export const authRegister = (body) =>
  handlePOSTRequest("authRegister", body, false, false, true);
export const getProfile = (body) => handlePOSTRequest("getProfile", body);

export const activateWallet = (body) =>
  handlePOSTRequest("activateWallet", body);

export const getInfoWallet = (body) => handlePOSTRequest("getInfoWallet", body);

export const getInfoPoints = () => handleGETRequest("getInfoPoints");

export const getStatusWallet = (body) =>
  handlePOSTRequest("getStatusWallet", body);

export const getInboxMessage = (query) =>
  handleGETRequest("getInboxMessage", false, query);

export const updateProfile = (body) => handlePOSTRequest("updateProfile", body);

export const authorizeToken = (body) =>
  handlePOSTRequest("authorizeToken", body, false, false, true);

export const getRefreshToken = (body) =>
  handlePOSTRequest("getRefreshToken", body);

export const authLogout = () => handlePOSTRequest("authLogout");

//BLOGS
export const getArticleList = () =>
  handleGETRequest("getArticleList", false, false, true);

export const getArticleMedia = ({ id }) =>
  handleGETRequest("getArticleMedia", { id }, false, true);

//PROMO
export const getListPromo = (query) =>
  handleGETRequest("getListPromo", false, query);

export const getDetailPromo = (query) =>
  handleGETRequest("getDetailPromo", false, query);

//FLIGHT
export const getSearchAirport = () => handleGETRequest("getSearchAirport");

export const getListSearchFlight = (body) =>
  handlePOSTRequest("getListSearchFlight", body);

export const getDetailSearchFlight = (body) =>
  handlePOSTRequest("getDetailSearchFlight", body);

export const createBookingFlight = (body) =>
  handlePOSTRequest("createBookingFlight", body);

//HOTEL
export const getListSearchHotel = (body) =>
  handlePOSTRequest("getListSearchHotel", body);

export const getSearchDestinationHotel = (query) =>
  handleGETRequest("getSearchDestinationHotel", false, query);

export const getDetailHotel = (body) =>
  handlePOSTRequest("getDetailHotel", body);

export const getHotelReview = (body) =>
  handlePOSTRequest("getHotelReview", body);

export const getHotelFacilities = (body) =>
  handlePOSTRequest("getHotelFacilities", body);

export const getHotelTag = (body) => handlePOSTRequest("getHotelTag", body);

export const createBookingHotel = (body) =>
  handlePOSTRequest("createBookingHotel", body);

export const createHotelReview = (body) =>
  handlePOSTRequest("createHotelReview", body);

//TOUR
export const getListRegion = (query) =>
  handleGETRequest("getListRegion", false, query);

export const getListSearchTour = (query) =>
  handleGETRequest("getListSearchTour", false, query);

export const getListTour = (body) => handlePOSTRequest("getListTour", body);

export const getFilterTour = () =>
  handleGETRequest("getFilterTour", false, false, true);

export const getDetailTour = (query) =>
  handleGETRequest("getDetailTour", false, query);

export const getTourRule = (query) =>
  handleGETRequest("getTourRule", false, query);

export const getPriceCalculate = (body) =>
  handlePOSTRequest("getPriceCalculate", body);

export const createBookingTour = (body) =>
  handlePOSTRequest("createBookingTour", body);

export const getBookingContinue = (query) =>
  handleGETRequest("getBookingContinue", false, query);

export const getListBookingHistory = (query) =>
  handleGETRequest("getListBookingHistory", false, query);

export const getDetailBooking = (query) =>
  handleGETRequest("getDetailBooking", false, query);

//PAYMENT
export const getPaymentMethod = (query) =>
  handleGETRequest("getPaymentMethod", false, query);

export const createInvoice = (body) => handlePOSTRequest("createInvoice", body);

export const getPricePromo = (body) => handlePOSTRequest("getPricePromo", body);
export const cancelBooking = (body) => handlePOSTRequest("cancelBooking", body);

export const getLastSearch = (body) => handlePOSTRequest("getLastSearch", body);
