const apiList = [
  {
    url: process.env.NEXT_PUBLIC_BASE_URL_API_BOOKING,
    children: [
      {
        url: "v2",
        children: [
          {
            url: "tours",
            children: [
              {
                url: "searches",
                children: [
                  {
                    url: "region",
                    method: "GET",
                    name: "getListRegion",
                  },
                ],
              },
              {
                url: "availabilities",
                children: [
                  {
                    url: "search",
                    method: "POST",
                    name: "getListTour",
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
];

export default apiList;
