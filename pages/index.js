import HeadSEO from "../components/seo/head";
import styles from "../styles/global.module.scss";
import Link from "next/link";
import BaseLayout from "../components/layout/BaseLayout";
import ExploreServiceCard from "../components/exploreService/exploreServiceCard";
import TrendingDestinations from "../components/Slider/trendingDestinationSlider";
import TopDestinations from "../components/Slider/topDestinationSlider";
import Hero from "../components/hero";

export default function Home(props) {
  const isDesktop = props.screenWidth > 1024;
  const { screenWidth } = props;
  return (
    <div className={styles.container}>
      <HeadSEO title={"Ankasa | Home"} description={"Home"} />
      <BaseLayout screenWidth={screenWidth} headerMobileTitle={"Explore"}>
        {isDesktop ? <Hero /> : <ExploreServiceCard />}
        <TrendingDestinations isDesktop={isDesktop} />
        <TopDestinations isDesktop={isDesktop} />
      </BaseLayout>
    </div>
  );
}
