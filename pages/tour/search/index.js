import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { getSearchTour } from "../../../api/helper";
import BaseLayout from "../../../components/layout/BaseLayout";
import Pagination from "../../../components/Pagination/Pagination";
import InfiniteScroll from "react-infinite-scroll-component";
import HeadSEO from "../../../components/seo/head";
import TourCard from "../../../components/tour/tourCard";
import Style from "../tour.module.scss";

export default function SearchTour(props) {
  const { screenWidth } = props;
  const isDesktop = screenWidth > 1024;
  const router = useRouter();
  const searchQuery = router && router.query && router.query.r;
  const [searchTourData, setSearchTourData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [maxPage, setMaxPage] = useState(1);
  const [countData, setCountData] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [hasMore, setHasMore] = useState(false);
  const [isLoad, setIsLoad] = useState(false);
  const [isLoadingSearchTour, setIsLoadingSearchTour] = useState(true);
  const [filter, setFilter] = useState(false);
  const [sortValue, setSortValue] = useState(1);
  const [searchValue, setSearchValue] = useState("");

  const handleGetSearchTour = useCallback(async () => {
    const data = await getSearchTour(currentPage, searchQuery);
    const updateSearchTourData = isDesktop
      ? [...data.dataSearchTour]
      : data.paging.currentPage === 1
      ? [...data.dataSearchTour]
      : [...searchTourData, ...data.dataSearchTour];
    setSearchTourData(updateSearchTourData);
    setMaxPage(data.paging.maxPage);
    setCountData(data.paging.countData);
    setTotalData(data.paging.totalData);
    setHasMore(data.paging.hasMore);
    setIsLoad(true);
    setIsLoadingSearchTour(false);
    if (isDesktop) {
      window.scrollTo(0, 0);
    }
  }, [currentPage]);

  useEffect(() => {
    setSearchTourData([]);
    setCountData(0);
    setTotalData(0);
    setHasMore(false);
    setCurrentPage(1);
    // setSortValue(1);
    // setFilter(true);
  }, [isDesktop]);

  useEffect(() => {
    handleGetSearchTour();
  }, [handleGetSearchTour]);

  const handleChangePage = async ({ selected: selectedPage }) => {
    setCurrentPage(selectedPage + 1);
  };

  const handleNext = async () => {
    if (hasMore) {
      setCurrentPage(currentPage + 1);
    }
  };
  console.log(searchTourData);

  return (
    <>
      <HeadSEO title="Ankasa Tour & Travel" description="Tour" />

      <BaseLayout screenWidth={screenWidth} headerMobileTitle={"Tour"}>
        {isDesktop ? (
          <main className="max-w-[1366px] h-full flex flex-row justify-between items-start gap-[12px] px-[65px] py-5 mx-auto">
            <div className={`${Style.left__content} shadow-md bg-white`}>
              Filter
            </div>

            <div className={Style.right__content}>
              <div className="w-full rounded-full shadow-md p-4 bg-white">
                Search for destination and region
              </div>

              <div className="w-full h-full py-5">
                {searchTourData && searchTourData.length > 0 ? (
                  <>
                    <div
                      className={`${Style.wrapper__popularTour} ${Style.resultSearch}`}
                    >
                      {searchTourData.map((item, index) => {
                        return (
                          <TourCard
                            item={item}
                            key={"tour" + index}
                            className={Style.resultSearch}
                            showIsPopular={true}
                          />
                        );
                      })}
                    </div>

                    <Pagination
                      pageCount={maxPage}
                      onPageChange={handleChangePage}
                      forcePage={currentPage - 1}
                    />
                  </>
                ) : (
                  ""
                )}
              </div>
            </div>
          </main>
        ) : (
          <>
            <div>
              {searchTourData && searchTourData.length > 0 ? (
                <>
                  <InfiniteScroll
                    dataLength={searchTourData ? searchTourData.length : 0}
                    next={handleNext}
                    hasMore={hasMore}
                    loader={
                      <div className="w-full my-4 text-center"> Loading...</div>
                    }
                    endMessage={""}
                    className="flex flex-wrap container-content  justify-center xl:justify-start xl:padding-container-content"
                  >
                    {searchTourData.length > 0 &&
                      searchTourData.map((item, index) => {
                        return (
                          <TourCard
                            item={item}
                            key={"tour" + index}
                            className="w-full"
                          />
                        );
                      })}
                  </InfiniteScroll>
                </>
              ) : (
                ""
              )}
            </div>
          </>
        )}
      </BaseLayout>
    </>
  );
}
