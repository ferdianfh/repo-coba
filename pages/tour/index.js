import { useEffect, useState } from "react";
// import { getListRegion, getListTour } from "../../api";
import HeadSEO from "../../components/seo/head";
import BaseLayout from "../../components/layout/BaseLayout";
import BannerTour from "../../components/tour/bannerTour";
import { DestinationCard } from "../../components/tour/destinationCard";
import Style from "../../components/tour/tour.module.scss";
import { getMainRegion, getPopularTour } from "../../api/helper";
import PopularTour from "../../components/tour/popularTour";

export default function Tour(props) {
  const isDesktop = props.screenWidth > 1024;
  const [dataRegion, setDataRegion] = useState([]);
  const [dataPopularTour, setDataPopularTour] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      const mainRegion = await getMainRegion();
      const popularTour = await getPopularTour();
      setDataRegion(mainRegion);
      setIsLoading(false);
      setDataPopularTour(popularTour);
      setIsLoading(false);
    };

    fetchData();
  }, []);

  // console.log(dataRegion);
  // console.log(dataPopularTour);

  return (
    <>
      <HeadSEO title="Ankasa Tour & Travel" description="Tour" />
      <BaseLayout screenWidth={props.screenWidth} headerMobileTitle="Tour">
        <BannerTour />
        <div className={Style.titleSection}>Destination</div>
        <DestinationCard
          Style={Style}
          screenWidth={props.screenWidth}
          isLoading={isLoading}
          data={dataRegion}
        />
        <div className={`${Style.titleSection} ${Style.title2}`}>
          Popular Tour
        </div>
        <PopularTour data={dataPopularTour} isLoading={isLoading} />
      </BaseLayout>
    </>
  );
}

export async function getServerSideProps(context) {
  // triggered every request
  const mainRegion = await getMainRegion();
  const popularTour = await getPopularTour();
  // console.log(mainRegion);
  return {
    props: { mainRegion, popularTour },
  };
}
