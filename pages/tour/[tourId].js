import BaseLayout from "../../components/layout/BaseLayout";
import HeadSEO from "../../components/seo/head";

export default function DetailTour(props) {
  return (
    <>
      <HeadSEO title="Ankasa | Detail Tour" description="Detail Tour" />
      <BaseLayout screenWidth={props.screenWidth}>
        <div>Detail Tour</div>
      </BaseLayout>
    </>
  );
}
