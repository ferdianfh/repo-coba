import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import nprogress from "nprogress";
import { handleGenerateDeviceId } from "../api/helper";
import "../styles/globals.scss";

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  const [screenWidth, setScreenWidth] = useState("");
  const router = useRouter();

  const generateDeviceId = useCallback(async () => {
    await handleGenerateDeviceId();
  }, []);

  useEffect(() => {
    const deviceId = Cookies.get("deviceId");
    let isValidCookiesVersion = true;

    if (isValidCookiesVersion) {
      if (!deviceId) {
        generateDeviceId();
      }
      // mendapatkan lebar layar
      setScreenWidth(screen.width);

      router.events.on("routeChangeStart", () => {
        nprogress.start();
      });
      router.events.on("routeChangeComplete", () => {
        nprogress.done();
      });
      return () => {
        nprogress.done();
      };
    }
  }, [generateDeviceId]);

  const screenResize = useCallback(async () => {
    // mendapatkan lebar layar saat resize layar
    setScreenWidth(screen.width);
  }, []);

  useEffect(() => {
    window.addEventListener("resize", screenResize);
    return () => {
      window.removeEventListener("resize", screenResize);
    };
  }, [screenResize]);

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <Component {...pageProps} screenWidth={screenWidth} />
        <ToastContainer />

        <ReactQueryDevtools />
      </QueryClientProvider>
    </>
  );
}

export default MyApp;
